# progas server

Aplicacion de servidor para la lectura de gases
requiere instalar framework adonisJS y NodeJS
- https://adonisjs.com/
- https://nodejs.org/es/


## Setup



```bash
cd progas_server
npm install
adonis serve 
```
## Configuracion

el la raiz del proyecto hay un archivo llamado .env . en este archivo estan los parametros de configuracion que se deben
establecer segun el servidor donde se instale.
