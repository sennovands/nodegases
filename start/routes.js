'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')
const User = use('App/Models/User')

Route.group(() => {

}).middleware(['guest'])

Route.group(() => {
  Route.any('*', ({view}) => view.render('welcome')).as('vuejs')
}).prefix('app')
Route.group(() => {
  Route.get('/', ({response}) => {
    return response.redirect('/app/dashboard', 301)
  })
  Route.get('/app/dashboard', ({view}) => view.render('welcome'));

// API ROUTER
  Route.get('/api/sensores', 'SensorController.list')
  Route.get('/api/gastipo/list', 'TipoGasController.list')
  Route.get('/api/niveles/list', 'NivelDuctoController.list')
  Route.get('/api/niveles/listselect', 'NivelDuctoController.listForSelect')
  Route.post('/api/sensor/detalle', 'SensorController.detalle')
  Route.post('/api/sensor/newdetalle', 'SensorController.newdetalle')
  Route.post('/api/arduino/lectura', 'SensorController.arduinoLectura')
  Route.get('/api/arduino/lectura/:codigo/:valor', "SensorController.arduinoLecturaGet")


}).middleware(['guest'])
