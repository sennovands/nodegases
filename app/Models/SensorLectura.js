'use strict'

const Model = use('Model')

class SensorLectura extends Model {
  static get table () {
    return 'sensorlecturas'
  }

  sensor(){
    return this.belongsTo('App/Models/Sensor','id', 'sensor_id');
  }

}

module.exports = SensorLectura
