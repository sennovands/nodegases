'use strict'

const Model = use('Model')

class GasTipo extends Model {

    static get table() {
        return 'gastipos'
    }

    sensor() {
        return this.hasMany('App/Models/Sensor', 'gastipo_id', 'id')
    }


}

module.exports = GasTipo
