'use strict'

const Model = use('Model')
const SensorLecturas = use('App/Models/SensorLectura');
const NivelDucto = use('App/Models/NivelDucto');

class Sensor extends Model {
  static get table() {
    return 'sensores'
  }

  gastipo() {
    return this.belongsTo('App/Models/GasTipo', 'gastipo_id', 'id');
  }

  nivelducto() {
    return this.belongsTo('App/Models/NivelDucto', 'nivelducto_id', 'id');
  }

  lecturas() {
    return this.hasMany('App/Models/SensorLectura', 'id', 'sensor_id')
  }
}

module.exports = Sensor
