'use strict'

const Model = use('Model')

class NivelDucto extends Model {
  static get table() {
    return 'nivelductos'
  }

  sensores() {
    return this.hasMany('App/Models/Sensor', 'id', 'nivelducto_id')
  }
  subniveles() {
    return this.hasMany('App/Models/NivelDucto', 'id', 'nivelducto_id')
  }

  nivelpadre() {
    return this.belongsTo('App/Models/NivelDucto', 'nivelducto_id', 'id')
  }


}

module.exports = NivelDucto
