'use strict'
const Sensores = use('App/Models/Sensor');
const Lecturas = use('App/Models/SensorLectura');
const Nivel = use('App/Models/NivelDucto');
const Database = use('Database');


class SensorController {

    interval = null;

    constructor({socket, request}) {
        this.socket = socket
        this.request = request

        this.interval=setInterval(
          async function() {
            await this.recursive()
          }
        )

    }

    async recursive(){
      const lista = await this.consultarSensores();
      this.socket.broadcastToAll('recibe', lista)
    }

    async onList(message) {
        const lista = await this.consultarSensores();
        this.socket.broadcastToAll('recibe', lista)

    }

    async onNewlectura(message){

      var datos = message.split("=");
      const lectura = new Lectura();
      var sensor = await Sensores.findByOrFail('codigo', datos[0]);
      lectura.sensor_id = sensor.id;
      lectura.valor = datos[1];
      lectura.valor_alerta = sensor.gastipo.nivel_alerta;
      lectura.valor_peligro = sensor.gastipo.nivel_peligro;
      await lectura.save();


      const lista = await this.consultarSensores();
      this.socket.broadcastToAll('recibe', lista)
    }

    async onListNiveles(message) {

        const lista = await this.consultarDuctos();
        this.socket.broadcastToAll('recibeducto', lista)

    }
    async onNewsensor(params){

        var operacion = 'agregado';
        var estado = 'fallo';
        var mensaje = '';
        if(params.codigo == null || params.codigo == ''){
            mensaje += '• <b>código</b> no puede estar vacío<br>'
        }
        if(params.nombre == null || params.nombre == ''){
            mensaje+="• <b>nombre</b> no puede estar vacío<br>"
        }
        if(mensaje.length == 0){
            try{
                var item;
                if(params.id == null){
                    item = new Sensores();
                }else{
                    operacion = "editado"
                    item = await Sensores.find(params.id);
                }
                item.codigo = params.codigo;
                item.nombre = params.nombre;
                item.puerto_analogo = params.puerto_analogo;
                item.puerto_digital = params.puerto_digital;
                item.nivelducto_id = params.nivel;
                item.gastipo_id = params.tipo_gas
                await item.save();

                //aviso a todos que cambio
                estado = 'ok';
                mensaje = "Sensor de gas <b>"+item.nombre+"</b> "+operacion+" correctamente";
                const lista = await this.consultarDuctos();
                this.socket.broadcastToAll('recibeducto', lista)
            }catch(e){

                estado = 'fallo';
                mensaje = e;
            }
        }else{
            estado = 'fallo'
        }

        const resultado = {'status': estado, 'title': 'Resultado operación', 'message': mensaje};
        //aviso al cliente que la operacion fue realizada
        this.socket.emit('op',resultado);
    }
    async onNewnivel(params){

        var estado = 'fallo';
        var mensaje = '';
        if(params.codigo == null || params.codigo == ''){
            mensaje += '• <b>código</b> no puede estar vacío<br>'
        }
        if(params.nombre == null || params.nombre == ''){
            mensaje+="• <b>nombre</b> no puede estar vacío<br>"
        }
        if(mensaje.length == 0){
            try{
                const item = new Nivel();
                item.codigo = params.codigo;
                item.nombre = params.nombre;
                item.descripcion = params.descripcion;
                await item.save();

                //aviso a todos que cambio
                estado = 'ok';
                mensaje = 'Nivel de ducto agregado correctamente';
                const lista = await this.consultarDuctos();
                this.socket.broadcastToAll('recibeducto', lista)
            }catch(e){
                estado = 'fallo';
                mensaje = e;
            }
        }else{
            estado = 'fallo'
        }

        const resultado = {'status': estado, 'title': 'Resultado operación', 'message': mensaje};
        //aviso al cliente que la operacion fue realizada
        this.socket.emit('op',resultado);
    }



    async onEdit(data) {
        var aux = data;
        var sensor = await Sensores.find(aux.id);
        sensor.deleted_at = aux.deleted_at != null ? null : new Date();
        const r = await sensor.save();
        //envio nuevamente la lista
        const lista = await this.consultarSensores();
        this.socket.broadcastToAll('recibe', lista)
    }

    async onEditnivel(data) {
        var aux = data;
        var row = await Nivel.find(aux.id);
        row.deleted_at = aux.deleted_at != null ? null : new Date();
        const r = await row.save();
        const lista = await this.consultarDuctos();
        this.socket.broadcastToAll('recibeducto', lista)
    }


    async onNivelDucto(message) {
        const lista = await this.consultarDuctos();
        this.socket.broadcastToAll('recibeducto', lista)
    }


    /**
     * Consulta los sensores y su ultima lectura
     * @returns {Array}
     */
    async consultarSensores() {

        var lista = await Sensores.query()
            .with('gastipo')
            .with('nivelducto')
            .orderBy('deleted_at', 'asc')
            .fetch();
        var lista = lista.toJSON();

        await Promise.all(lista.map(async (item) => {
            item.lectura = await Lecturas.query().where('sensor_id', item.id)
                .orderBy('created_at', 'desc').first();
            await item.load('nivelducto');
        }))
        await Promise.all(lista.map(async (item) => {
            await item.load('nivelducto');
        }))

        return lista;
    }

    /**
     *
     * @returns {Array}
     */
    async consultarDuctos(){
        var lista = await Nivel.query()
            .fetch();
        return lista.toJSON();
    }
}

module.exports = SensorController
