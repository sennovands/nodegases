'use strict'

const Nivel = use ('App/Models/NivelDucto')


class NivelDuctoController {

  async list({params, response }){
      const lista = await Nivel.query().with('subniveles')
        .with('nivelpadre')
        .fetch();

    return response.json({
      status: 'success',
      data: lista.toJSON()
    });
  }

    async listForSelect({params, response }){
        const lista = await Nivel.query().with('subniveles')
            .with('nivelpadre')
            .select('nombre as text', 'id as value')
            .fetch();

        return response.json({
            status: 'success',
            data: lista.toJSON()
        });
    }

  async Tree({params, response }){
    const lista = await Nivel.query().with('subniveles')
      .with('nivelpadre')
      .whereNull('nivelducto_id')
      .fetch();

    return response.json({
      status: 'success',
      data: lista.toJSON()
    });
  }

  async eliminar({params, response }){
    const nivel = Nivel.findById(params.id)
  }

  async agregar({params, response}){
      const item = new Nivel();
      item.codigo = params.codigo;
      item.nombre = params.nombre;
      item.descripcion = params.descripcion;

      await item.save();
      return response.json({
          status: 'success',
          data: item.toJSON()
      });
  }
}

module.exports = NivelDuctoController
