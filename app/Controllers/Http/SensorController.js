'use strict'

const Ws = use('Ws')
const wsc = require('adonis-websocket-client')
const Env = use('Env')
const Sensores = use('App/Models/Sensor')
const Lectura = use('App/Models/SensorLectura')

class SensorController {

  async list({params, response}) {
    const lista = await Sensores.query()
      .with('gastipo')
      .fetch();

    return response.json({
      status: 'success',
      data: lista.toJSON()
    });
  }


  async guardar({params, response}) {
    const sensor = new Sensores();
    sensor.nombre = params.nombre;
    sensor.nivelducto_id = params.nivel;
  }

  async detalle({request, response}) {
    try {
      const sensor = request.all();
      console.log(sensor);
      const lecturas = await Lectura.query()
        .select('id', 'created_at', 'valor', 'valor_alerta', 'valor_peligro', 'estado')
        .where('sensor_id', sensor.id)
        .orderBy('created_at', 'desc')
        .offset(0).limit(100)
        .fetch();

      return response.json({lecturas: lecturas.toJSON()});
    } catch (e) {
      return response.json({
        error: e,
        request: request,
        response: response,
        fields: sensor,
      });
    }
  }

  async newdetalle({request, response}) {
    const sensor = request.all();
    const lecturas = await Lectura.query()
      .select('id', 'created_at', 'valor', 'valor_alerta', 'valor_peligro', 'estado')
      .where('sensor_id', sensor.id)
      .where('created_at', '>', sensor.ultimodetalle)
      .orderBy('created_at', 'desc')
      .offset(0).limit(100)
      .fetch();

    return response.json({lecturas: lecturas.toJSON()});
  }


  async arduinoLectura({request, response}) {
    var  client = null;
    const io = wsc('http://localhost:3333', {})
    try {
      var sensor = await Sensores.findByOrFail('codigo', request.body.codigo);
      if (sensor != null) {
        const lectura = new Lectura();
        lectura.sensor_id = sensor.id;
        lectura.valor = request.body.valor;
        lectura.valor_alerta = sensor.gastipo.nivel_alerta;
        lectura.valor_peligro = sensor.gastipo.nivel_peligro;
        await lectura.save();
        client = Ws.getChannel("sensor");
        io.connect();
        io.emit('list', '');

        response.json({status: 'ok', message: 'lectura agregada id: '+lectura.id+'. al sensor ' + sensor.codigo + ' - ' + sensor.nombre, data: client})
      } else {
        response.json({status: 'error', message: 'Sensor no encontrado'})
      }
    }
    catch (exception) {
      response.json({status: 'error', message: 'lectura no agregada.' + exception.toString(),data: client})
    }
  }

  async arduinoLecturaGet({params, response}) {
    console.log(params);
    var sensor = await Sensores.findByOrFail('codigo', params.codigo);
    if(sensor != null){
      const lectura = new Lectura();
      lectura.sensor_id = sensor.id;
      lectura.valor = params.valor;
      lectura.valor_alerta = sensor.gastipo.nivel_alerta;
      lectura.valor_peligro = sensor.gastipo.nivel_peligro;
      await lectura.save();

      

      return response.json({
        status: 'success',
        data: lectura.toJSON()
      });

    }else{
      return response.json({
        status: 'success',
        data: "Sensor not found"
      });
    }



  }

}


module.exports = SensorController
