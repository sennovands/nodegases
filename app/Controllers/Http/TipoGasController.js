'use strict'

const GasTipo = use('App/Models/GasTipo')

class TipoGasController {

  async list({params, response }){
    const lista = await GasTipo.query()
      .select('nombre as text','id as value')
      .fetch();

    return response.json({
      status: 'success',
      data: lista.toJSON()
    });
  }
}

module.exports = TipoGasController
