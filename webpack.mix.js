const mix = require('laravel-mix');


mix.autoload({
  'jquery': ['jQuery', '$'],
});

mix.js([
  'node_modules/jquery/dist/jquery.js',
  'resources/js/app.js'
], 'public/js/app.js')
  .sass('resources/sass/app.scss', 'public/css')
  .options({
    processCssUrls: false
  })
  .copy('node_modules/font-awesome/fonts/', 'public/fonts')
    .copy('resources/img/', 'public/img')
  .copy('node_modules/material-icons/iconfont/', 'public/css/material-icons/iconfont')
    .copy('node_modules/element-ui/lib/theme-chalk/fonts', 'public/css/fonts')
;

