// store.js
import Vue from 'vue'
import Puex from 'puex'

Vue.use(Puex)

const store = new Puex({
    state: {
        ws: null,
        ws_sensores:null
    },
    mutations: {
        initSocket(state, ws){
            state.ws = ws
            //state.ws_sensores = state.ws.subscribe('sensor')
        }
    },
    actions: {},
    plugins: []
})

export default store