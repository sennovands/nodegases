window.$ = window.jQuery = require('jquery');
require('bootstrap')
window.Vue = require('vue');


// LIBRERIAS
import 'babel-polyfill'
import Femr from './Femr'

var VueResource = require('vue-resource');
Vue.use(VueResource);

import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Vuex from 'vuex'

Vue.use(Vuex)

import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);

import Vuesax from 'vuesax'

Vue.use(Vuesax)

import Puex from 'puex'

Vue.use(Puex)
import store from './store'


import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/es'

Vue.use(ElementUI, {locale})

//import views
import App from './components/App'
import Dashboard from './components/Dashboard'
import Sensores from './components/Sensores'
import SensorNuevo from './components/SensorNuevo'
import Niveles from './components/Niveles'
import NivelNuevo from './components/NivelNuevo'
import SensorDetalle from './components/SensorDetalle'

// ROUTERS
const router = new VueRouter({
    mode: 'history',
    routes: [

        {
            path: '/app/dashboard',
            name: 'dashboard',
            component: Dashboard
        },
        {
            path: '/app/sensores',
            name: 'sensores',
            component: Sensores
        },
        {
            path: '/app/sensores/agregar',
            name: 'sensor-nuevo',
            component: SensorNuevo
        },
        {
            path: '/app/niveles',
            name: 'niveles',
            component: Niveles
        },
        {
            path: '/app/niveles/agregar',
            name: 'nivel-nuevo',
            component: NivelNuevo
        },
        {
            path: '/app/sensores/detalle',
            name: 'sensor-detalle',
            component: SensorDetalle
        },
        {
            path: '/app/*',
            component: Dashboard
        }
    ],
});
//WEBSOCKET
import Ws from '@adonisjs/websocket-client'

//Include prototypes
Vue.prototype.$femr = Femr;


/// MAIN VUE APP
const app = new Vue({
    el: '#app',
    store,
    data: {
        menu: 'dashboard'
    },
    components: {App},
    created() {
        this.menu = 'dashboard';
    },
    methods: {
        socketInit() {
            const ws = Ws('ws://'+this.$femr.host())
            ws.on('open', () => {
                console.log('open');
            })

            ws.on('close', () => {
                console.log('close');
            })

            ws.on('error', () => {
                location.reload();
            })

            ws.connect()
            return ws;
        },
        getClient() {
            try {
                this.$store.ws.close();
            } catch (e) {
            }
            try {
                this.$store.commit('initSocket', this.socketInit());
                var client = this.$store.state.ws.subscribe('sensor');
                console.log('subscripcion iniciada');
                return client;
            } catch (e) {
                console.log("Suscripcion error. " + e)
                return null;
            }
        },
        notificacion(op) {
            var tipo;
            switch (op.status) {
                case 'ok':
                    tipo = 'success';
                    break;
                case 'fallo':
                    tipo = 'warning';
                    break;
                case 'error':
                    tipo = 'error';
                    break;
                default :
                    tipo = 'info';
                    break;
            }
            this.$notify({
                title: op.title,
                message: op.message,
                dangerouslyUseHTMLString: true,
                type: tipo,
                duration: 6000
            });
        },
        confirmacion(titulo, mensaje, ok, cancel) {
            var _this = this;
            this.$confirm(mensaje, titulo, {
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                type: 'warning'
            }).then(() => {
                if (ok != null)
                    ok.call(_this);
            }).catch(() => {
                if (cancel != null)
                    cancel.call(_this);

            });
        }
    },
    router
});

