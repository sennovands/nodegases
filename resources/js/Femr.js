class Femr {

  constructor() {
    var _this = this;

  }


  serve() {
    return window.location.protocol + '//' + window.location.host;
  }

  asset(url) {
    return window.location.protocol + '//' + window.location.host + '/' + url;
  }

  api(url) {
    return window.location.protocol + '//' + window.location.host + '/api/' + url;
  }

  web(url) {
    return window.location.protocol + '//' + window.location.host + '/' + url;
  }
  host(){
    return window.location.host;
  }

  alert(titulo, mensaje, onSuccess = function () {
    alert('OK')
  }, onCancel = function () {
    alert('Cancel')
  }){
    return new Modal(titulo, mensaje, onSuccess, onCancel);
  }


}

class Modal {

  constructor(titulo, mensaje, onSuccess = function () {
  }, onCancel = function () {
  }) {
    this.languaje = {};
    this.titulo = titulo;
    this.mensaje = mensaje;
    this.onSuccess = onSuccess;
    this.onCancel = onCancel;
    jQuery("#modal_titulo").html(this.titulo);
    jQuery("#modal_mensaje").html(this.mensaje);
    jQuery("#modal_aceptar").html("Continuar");
    const _this = this;
    jQuery("#modal_aceptar").on('click', function () {
      _this.onSuccess.call(this);
    });
    jQuery("#modal_cancelar").on('click', function () {
      _this.onCancel.call(this);
    });

  }

  setOkMessage(message) {
    jQuery("#modal_aceptar").html(message);
    return this;
  }

  success(callback) {
    this.onSuccess = callback;
    const _this = this;
    jQuery("#modal_aceptar").on('click', function () {
      _this.onSuccess.call(this);
    });
    return this;
  }

  cancel(callback) {
    this.onCancel = callback;
    const _this = this;
    jQuery("#modal_cancelar").on('click', function () {
      _this.onCancel.call(this);
    });
    return this;
  }

  show(){
    jQuery("#modal_confirmacion").modal('show');
    return this;
  }



}

export default new Femr();
